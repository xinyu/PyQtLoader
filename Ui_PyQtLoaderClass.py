#! /usr/bin/python

######################################################################
#
#      Project: PyQtLoader
#      Version: 1.0
#         Date: Thu May 28 00:00:00 2009
#       Author: Xin-Yu Lin
#       E-mail: xinyu0123@gmail.com
#      WebSite: http://nxforce.blogspot.com
#  Description: Python Script Loader
#
######################################################################

############### import modules #######################################

from PyQt4 import QtCore, QtGui

############### Ui_PyQtLoaderClass start #############################

class Ui_PyQtLoaderClass(object):
	def setupUi(self, PyQtLoaderClass):
		PyQtLoaderClass.resize(500,50)
		
		# Export Icon
		self.fileIcon = QtGui.QIcon("icon/File.png")
		self.programIcon = QtGui.QIcon("icon/Program.png")		
		
		# export button
		self.pushButton_export = QtGui.QPushButton(PyQtLoaderClass)
		self.pushButton_export.setGeometry(QtCore.QRect(10, 10, 35, 30))
		self.pushButton_export.setObjectName("pushButton_path")
		self.pushButton_export.setIcon(self.fileIcon)
		
		# export DirDialog
		self.fileDialog_export = QtGui.QFileDialog(PyQtLoaderClass)
		
		# export lineEdit
		self.lineEdit_path = QtGui.QLineEdit(PyQtLoaderClass)
		self.lineEdit_path.setGeometry(QtCore.QRect(55, 10, 315, 30))
		self.lineEdit_path.setObjectName("lineEdit_export")
		
		# 'Generate' button
		self.pushButton_generate = QtGui.QPushButton(PyQtLoaderClass)
		self.pushButton_generate.setGeometry(QtCore.QRect(380, 10, 110, 30))
		self.pushButton_generate.setObjectName("pushButton_generate")
		self.pushButton_generate.setIcon(self.programIcon)

		self.retranslateUi(PyQtLoaderClass)
		QtCore.QObject.connect(self.pushButton_generate, QtCore.SIGNAL("clicked()"), PyQtLoaderClass.onGenerateClicked)
		QtCore.QObject.connect(self.pushButton_export, QtCore.SIGNAL("clicked()"), PyQtLoaderClass.onExportClicked)
		QtCore.QMetaObject.connectSlotsByName(PyQtLoaderClass)

	def retranslateUi(self, PyQtLoaderClass):
		PyQtLoaderClass.setWindowTitle(QtGui.QApplication.translate("PyQtLoaderClass", "PyQtLoader", None, QtGui.QApplication.UnicodeUTF8))
		self.pushButton_generate.setText(QtGui.QApplication.translate("PyQtLoaderClass", "Generate", None, QtGui.QApplication.UnicodeUTF8))

############### Ui_PyQtLoaderClass end ###############################
