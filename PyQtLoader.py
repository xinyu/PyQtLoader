#! /usr/bin/python

######################################################################
#
#      Project: PyQtLoader
#      Version: 1.0
#         Date: Thu May 28 00:00:00 2009
#       Author: Xin-Yu Lin
#       E-mail: xinyu0123@gmail.com
#      WebSite: http://nxforce.blogspot.com
#  Description: Python Script Loader
#
######################################################################

############### import modules #######################################

import sys
import PyQtLoaderClass
from PyQt4 import QtCore, QtGui

############### PyQtLoader start #####################################

if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	PyQtLoaderObj = PyQtLoaderClass.PyQtLoaderClass()
	PyQtLoaderObj.show()
	sys.exit(app.exec_())

############### PyQtLoader start #####################################
