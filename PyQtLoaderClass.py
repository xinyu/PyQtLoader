#! /usr/bin/python

######################################################################
#
#      Project: PyQtLoader
#      Version: 1.0
#         Date: Thu May 28 00:00:00 2009
#       Author: Xin-Yu Lin
#       E-mail: xinyu0123@gmail.com
#      WebSite: http://nxforce.blogspot.com
#  Description: Python Script Loader
#
######################################################################

############### import modules #######################################

import os
import subprocess
import py_compile
import Ui_PyQtLoaderClass
from PyQt4 import QtCore, QtGui

############### PyQtLoaderClass Start ################################

class PyQtLoaderClass(QtGui.QWidget):
	def __init__(self, parent = None):
		QtGui.QWidget.__init__(self, parent)
		self.ui = Ui_PyQtLoaderClass.Ui_PyQtLoaderClass()
		self.ui.setupUi(self)
		self.filePath = ''

	def onGenerateClicked(self):
		if(self.filePath == ''):
			self.onExportClicked()

		py_compile.compile(self.filePath)

		splitext = os.path.splitext(self.filePath)
		srcFileName = splitext[0]+'.c'
		file = open(srcFileName, 'w')
		file.write("#include <stdio.h>\nint main(void){ system(\"python ./" + os.path.basename(self.filePath) + "\");return 0;}\n")
		file.close()
		os.system("gcc -o "+splitext[0]+" "+srcFileName)
		os.remove(srcFileName)

		messageBox = QtGui.QMessageBox('Generated Successfully!', 'What do you want to do next?', QtGui.QMessageBox.Information, 0, 0, 0  )

		actionRun = messageBox.addButton("Run Program", QtGui.QMessageBox.ActionRole);
		actionOpen = messageBox.addButton("Open Folder", QtGui.QMessageBox.ActionRole);
		actionClose = messageBox.addButton("Close", QtGui.QMessageBox.ActionRole);

		reply = messageBox.exec_()

		if messageBox.clickedButton() == actionRun:
			subprocess.call(self.filePath)
			exit(0)

		if messageBox.clickedButton() == actionOpen:
			subprocess.Popen(['nautilus' , os.path.dirname(self.filePath)])

		if messageBox.clickedButton() == actionClose:
			exit(0)

	def onExportClicked(self):
		filePath = self.ui.fileDialog_export.getOpenFileName(self, 'Open File', QtCore.QDir.homePath()+'/Desktop', "python(*.py)")
		self.ui.lineEdit_path.setText(filePath)
		self.filePath = str(filePath)


############### PyQtLoaderClass End ##################################
